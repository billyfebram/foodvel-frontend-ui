import { SPINNER_CONFIG } from './../app.constants';
import { Component, OnInit, Input } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { LoginModalService, AccountService, Account } from 'app/core';
import { IArticle } from 'app/shared/model/article.model';
import { Subscription } from 'rxjs';
import { ITEMS_PER_PAGE } from 'app/shared';
import { ArticleService } from 'app/entities/article';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit {
  account: Account;
  modalRef: NgbModalRef;
  currentAccount: any;
  articles: IArticle[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  type: any;
  spinners: any;
  loading: boolean;

  constructor(
    private accountService: AccountService,
    private loginModalService: LoginModalService,
    private eventManager: JhiEventManager,
    protected articleService: ArticleService,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected activatedRoute: ActivatedRoute,
    protected dataUtils: JhiDataUtils,
    protected router: Router
  ) {
    this.type = 'ALL';
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.loading = true;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.type = data.pagingParams.type === undefined ? 'ALL' : data.pagingParams.type;
    });
  }

  ngOnInit() {
    this.spinners = Object.keys(SPINNER_CONFIG).map(key => {
      return {
        name: key,
        divs: Array(SPINNER_CONFIG[key].divs).fill(1),
        class: SPINNER_CONFIG[key].class
      };
    });

    this.loadAll();
  //   this.router.events.subscribe(val => {
  //     console.log(val);
  //     if (val instanceof NavigationEnd) {
  //       // console.log(val);
  //     //  this.loadPage(0);
  //     }
  //  });
  }

  registerAuthenticationSuccess() {
    this.eventManager.subscribe('authenticationSuccess', message => {
      this.accountService.identity().then(account => {
        this.account = account;
      });
    });
  }

  isAuthenticated() {
    return this.accountService.isAuthenticated();
  }

  login() {
    this.modalRef = this.loginModalService.open();
  }

  loadAll() {
    this.loading = true;
    this.articleService
      .queryExcludeContent({
        page: this.page - 1,
        size: this.itemsPerPage,
        type: this.type,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IArticle[]>) => this.paginateArticles(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  trackId(index: number, item: IArticle) {
    return item.id;
  }

  transition() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.type = params.type;
      if (this.type === undefined) {
        this.type = 'ALL';
      }

      this.router.navigate(['/'], {
        queryParams: {
          page: this.page,
          size: this.itemsPerPage,
          type: this.type,
          sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }
      });
      this.loadAll();
    });
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateArticles(data: IArticle[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.articles = data;
    this.loading = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
