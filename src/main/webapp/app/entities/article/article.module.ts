import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FodvelCmsSharedModule } from 'app/shared';
import {
  // ArticleComponent,
  ArticleDetailComponent
  // ArticleUpdateComponent,
  // ArticleDeletePopupComponent,
  // ArticleDeleteDialogComponent,
  // articleRoute,
  // articlePopupRoute
} from './';
import { articleRoute, articlePopupRoute } from './article.route';

const ENTITY_STATES = [...articleRoute, ...articlePopupRoute];

@NgModule({
  imports: [FodvelCmsSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ArticleDetailComponent
  ],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FodvelCmsArticleModule {}
