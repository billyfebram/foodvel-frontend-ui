import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IArticle } from 'app/shared/model/article.model';
import { SPINNER_CONFIG } from 'app/app.constants';

@Component({
  selector: 'jhi-article-detail',
  templateUrl: './article-detail.component.html'
})
export class ArticleDetailComponent implements OnInit {
  article: IArticle;
  spinners: any;
  loading: any;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.loading = true;
    this.spinners = Object.keys(SPINNER_CONFIG).map(key => {
      return {
        name: key,
        divs: Array(SPINNER_CONFIG[key].divs).fill(1),
        class: SPINNER_CONFIG[key].class
      };
    });
    this.activatedRoute.data.subscribe(({ article }) => {
      this.loading = false;
      this.article = article;
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }
  previousState() {
    window.history.back();
  }
}
