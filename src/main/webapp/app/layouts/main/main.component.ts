import { SPINNER_CONFIG } from './../../app.constants';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot, NavigationEnd, NavigationError, ActivatedRoute } from '@angular/router';

import { Title } from '@angular/platform-browser';
import { ArticleService } from 'app/entities/article';
import { HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { IArticle } from 'app/shared/model/article.model';
import { JhiParseLinks, JhiAlertService } from 'ng-jhipster';

@Component({
  selector: 'jhi-main',
  templateUrl: './main.component.html'
})
export class JhiMainComponent implements OnInit {
  page: any;
  itemsPerPage: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  links: any;
  totalItems: any;
  articles: IArticle[];
  routeData: any;
  loading: boolean;
  spinners: any;

  constructor(
    private titleService: Title,
    private router: Router,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected activatedRoute: ActivatedRoute,
    protected articleService: ArticleService) {
      this.loading = true;
      this.routeData = this.activatedRoute.data.subscribe(data => {
        this.page = 0;
        // this.page = data.pagingParams.page;
        // this.previousPage = data.pagingParams.page;
        // this.reverse = data.pagingParams.ascending;
        // this.predicate = data.pagingParams.predicate;
      });
    }

  private getPageTitle(routeSnapshot: ActivatedRouteSnapshot) {
    let title: string = routeSnapshot.data && routeSnapshot.data['pageTitle'] ? routeSnapshot.data['pageTitle'] : 'fodvelCmsApp';
    if (routeSnapshot.firstChild) {
      title = this.getPageTitle(routeSnapshot.firstChild) || title;
    }
    return title;
  }

  loadAll() {
    this.loading = true;
    this.articleService
      .queryExcludeContent({
        page: this.page - 1,
        size: 4,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IArticle[]>) => this.paginateArticles(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateArticles(data: IArticle[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.articles = data;
    this.loading = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  ngOnInit() {
    this.spinners = Object.keys(SPINNER_CONFIG).map(key => {
      return {
        name: key,
        divs: Array(SPINNER_CONFIG[key].divs).fill(1),
        class: SPINNER_CONFIG[key].class
      };
    });
    this.loadAll();
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.titleService.setTitle(this.getPageTitle(this.router.routerState.snapshot.root));
      }
      if (event instanceof NavigationError && event.error.status === 404) {
        this.router.navigate(['/404']);
      }
    });
  }

  trackId(index: number, item: IArticle) {
    return item.id;
  }
}
