import { Route } from '@angular/router';

import { NavbarComponent } from './navbar.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const navbarRoute: Route = {
  path: '',
  component: NavbarComponent,
  resolve: {
    pagingParams: JhiResolvePagingParams
  },
  outlet: 'navbar'
};
