export interface IArticle {
  id?: string;
  title?: string;
  previewContent?: string;
  content?: string;
  tag?: string;
  type?: string;
  imageContentType?: string;
  image?: any;
  createdDate?: string;
  createdBy?: string;
}

export class Article implements IArticle {
  constructor(
    public id?: string,
    public title?: string,
    public previewContent?: string,
    public content?: string,
    public tag?: string,
    public type?: string,
    public imageContentType?: string,
    public image?: any,
    public createdDate?: string,
    public createdBy?: string
  ) {}
}
