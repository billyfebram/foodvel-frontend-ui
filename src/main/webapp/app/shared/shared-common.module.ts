import { NgModule } from '@angular/core';

import { FodvelCmsSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [FodvelCmsSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [FodvelCmsSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class FodvelCmsSharedCommonModule {}
